(require 'git-gutter-fringe)

(global-git-gutter-mode t)

(global-set-key (kbd "C-c n") 'git-gutter:next-hunk)
(global-set-key (kbd "C-c p") 'git-gutter:previous-hunk)

(provide 'init-git-gutter)
