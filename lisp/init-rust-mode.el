(autoload 'rust-mode "rust-mode.el" t)

(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))

(setq rust-format-on-save t)

(provide 'init-rust-mode)
