;; Disable auto saving
(setq auto-saving nil)

;; Trun off backup
(setq make-backup-files nil)

;; Setting default directory
(setq default-directory "~/")

;; Disable startup message
(setq inhibit-startup-message t)

;; Enable desktop save mode
(desktop-save-mode t)
;; Use a different file for terminal mode
(if (not (display-graphic-p))
    (progn (setq desktop-base-file-name ".emacs.terminal")
     (setq desktop-base-lock-name ".emacs.terminal.lock"))
  (server-start))

;; Keys setting on macOS
(when is-macos
  (setq mac-option-modifier 'alt)
  (setq mac-command-modifier 'meta)
  (global-set-key [kp-delete] 'delete-char))

;; Code style settings
(setq-default c-basic-offset 4)
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)

(global-set-key (kbd "C-c c") 'comment-or-uncomment-region)
(global-set-key (kbd "C-c g") 'goto-line)
(global-set-key (kbd "C-x k") 'kill-current-buffer)
(global-set-key (kbd "C-k") 'kill-whole-line)

(defun copy-word-at-point ()
  "Copy the word at point."
  (interactive)
  (let ((symbol (thing-at-point 'symbol)))
    (when symbol (kill-new symbol))))
(global-set-key (kbd "C-c o") 'copy-word-at-point)

(defun replace-word-at-point ()
  "Replace the word at point with the current clipboard content."
  (interactive)
  (let ((clipboard-content (current-kill 0))
        (bounds (bounds-of-thing-at-point 'symbol)))
    (if (and bounds clipboard-content)
        (progn
          (delete-region (car bounds) (cdr bounds))
          (insert clipboard-content))
      (message "No content at point or clipboard is empty."))))
(global-set-key (kbd "C-c r") 'replace-word-at-point)

;; Disable Ctrl-z
(global-unset-key (kbd "C-z"))
;; Disbale all manual keybindings
(global-unset-key (kbd "C-h"))

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(global-set-key (kbd "C-c u") 'upcase-region)
(global-set-key (kbd "C-c d") 'downcase-region)

(defun reopen-file ()
  "Reopen the current file."
  (interactive)
  (let ((buffer-name (buffer-name))
        (file-name (buffer-file-name))
        (point (point)))
    (kill-buffer buffer-name)
    (find-file file-name)
    (goto-char point)
    (message "Reopened file %s." file-name)))
(global-set-key (kbd "C-x f") 'reopen-file)

(provide 'init-base)
