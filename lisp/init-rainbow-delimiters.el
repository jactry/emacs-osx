(require 'rainbow-delimiters)

(define-globalized-minor-mode global-rainbow-delimiters-mode rainbow-delimiters-mode
  (lambda () (rainbow-delimiters-mode 1)))
(global-rainbow-delimiters-mode t)

(provide 'init-rainbow-delimiters)
