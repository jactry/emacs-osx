(autoload 'goto-line-preview "goto-line-preview.el" t)

(global-set-key [remap goto-line] 'goto-line-preview)

(provide 'init-goto-line-preview)
