(setq hs-allow-nesting t)
(defun turn-on-hs-minor-mode ()
  (hs-minor-mode)
   (diminish 'hs-minor-mode))
(add-hook 'c-mode-common-hook
          (lambda ()
            (turn-on-hs-minor-mode)))
(global-set-key (kbd "C-c h") 'hs-hide-block)
(global-set-key (kbd "C-c s") 'hs-show-block)
(global-set-key (kbd "C-c , h") 'hs-hide-all)
(global-set-key (kbd "C-c , s") 'hs-show-all)

(provide 'init-hs-minor-mode)
