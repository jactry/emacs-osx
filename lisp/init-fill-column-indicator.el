;; (require 'fill-column-indicator)

(setq-default display-fill-column-indicator-column 120)

(global-display-fill-column-indicator-mode)

(provide 'init-fill-column-indicator)
