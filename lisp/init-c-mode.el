;; wined3d code style
(setq c-default-style "bsd"
      c-backslash-max-column 1)
(c-set-offset 'arglist-cont-nonempty '++)
(c-set-offset 'statement-cont '0)
(c-set-offset 'case-label '+)

(defun sort-lines-by-length (reverse beg end)
  (interactive "P\nr")
  (save-excursion
    (save-restriction
      (narrow-to-region beg end)
      (goto-char (point-min))
      (let ((inhibit-field-text-motion t))
        (sort-subr reverse 'forward-line 'end-of-line nil nil
                   (lambda (l1 l2)
                     (apply #'> (mapcar (lambda (range) (- (cdr range) (car range)))
                                        (list l1 l2)))))))))

(global-set-key (kbd "C-c t") 'sort-lines-by-length)

(defun wrap-with-if-zero ()
  "Wrap the selected region with 'if (0) { ... }'"
  (interactive)
  (let ((beg (region-beginning))
        (end (region-end)))
    (goto-char end)
    (insert "\n}")
    (goto-char beg)
    (insert "if (0)\n{\n")))

(global-set-key (kbd "C-c w") 'wrap-with-if-zero)

(provide 'init-c-mode)
