;;; benchmark-init-loaddefs.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "benchmark-init" "benchmark-init.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from benchmark-init.el

(autoload 'benchmark-init/activate "benchmark-init" "\
Activate benchmark-init and start collecting data." t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "benchmark-init" '("benchmark-init/")))

;;;***

;;;### (autoloads nil "benchmark-init-modes" "benchmark-init-modes.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from benchmark-init-modes.el

(autoload 'benchmark-init/show-durations-tabulated "benchmark-init-modes" "\
Show the benchmark results in a sorted table." t nil)

(autoload 'benchmark-init/show-durations-tree "benchmark-init-modes" "\
Show durations in call-tree." t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "benchmark-init-modes" '("benchmark-init/")))

;;;***

(provide 'benchmark-init-loaddefs)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; benchmark-init-loaddefs.el ends here
