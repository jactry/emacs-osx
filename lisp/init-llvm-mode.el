(autoload 'llvm-mode "llvm-mode.el" t)

(add-to-list 'auto-mode-alist '("\\.ll\\'" . llvm-mode))

(provide 'init-llvm-mode)
