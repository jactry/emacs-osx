(require 'auto-highlight-symbol)

(global-auto-highlight-symbol-mode t)
(global-set-key (kbd "C-,") 'ahs-edit-mode)

(provide 'init-auto-highlight-symbol)
