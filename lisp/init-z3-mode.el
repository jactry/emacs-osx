(autoload 'z3-mode "z3-mode.el" t)

(setq auto-mode-alist (append '(("\\.smt[2]?$" . z3-mode)) auto-mode-alist))

(provide 'init-z3-mode)
