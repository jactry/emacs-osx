(defvar last-active-buffer nil)

(defun record-last-active-buffer ()
  (let ((buffer (current-buffer)))
    (unless (or (minibuffer-window-active-p (selected-window))
                (string-match-p "\\`\\*.*\\*\\'" (buffer-name buffer)))
      (setq last-active-buffer buffer))))

(add-hook 'post-command-hook 'record-last-active-buffer)

(defun switch-to-last-active-buffer ()
  (interactive)
  (when last-active-buffer
    (switch-to-buffer last-active-buffer)))

(defun vc-annotate-quit ()
  (interactive)
  (kill-buffer)
  (switch-to-last-active-buffer)
  (delete-other-windows)
  (jump-to-register :vc-annotate-fullscreen))

(defun vc-annotate-show-commit ()
  (interactive)
  (vc-annotate-show-diff-revision-at-line)
  (delete-other-windows))

(with-eval-after-load 'vc-annotate
  (define-key vc-annotate-mode-map (kbd "d") 'vc-annotate-show-commit)
  (define-key vc-annotate-mode-map (kbd "q") 'vc-annotate-quit))

(provide 'init-vc)
