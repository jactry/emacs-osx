(let ((dir (expand-file-name "lisp/plugins/themes" user-emacs-directory)))
  (add-to-list 'custom-theme-load-path dir)
  (let ((dirs (directory-files dir t directory-files-no-dot-files-regexp)))
    (dolist (dir dirs)
      (when (file-directory-p dir)
        (add-to-list 'custom-theme-load-path dir)
        (let ((subdirs (directory-files dir t directory-files-no-dot-files-regexp)))
          (dolist (subdir subdirs)
            (when (file-directory-p subdir)
              (add-to-list 'custom-theme-load-path subdir))))))))
(load-theme 'nord t)

(defvar preferred-font-name "IBM Plex Mono Text")

(if (and (display-graphic-p) (x-list-fonts preferred-font-name))
    (defvar font-setting preferred-font-name)
  (defvar font-setting ""))

(if (>= (display-pixel-height) 1080)
    ;; Desktop monitors
    (setq font-setting (concat font-setting " 12"))
  ;; Laptop monitors
  (setq font-setting (concat font-setting " 16")))

(set-frame-font font-setting)
(set-face-attribute 'default nil :font preferred-font-name)

;; Use 'y/n/p' instead of 'yes/no/p'
(fset 'yes-or-no-p 'y-or-n-p)

;; Set titlebar
(setq frame-title-format `(,(user-login-name) "@" ,(system-name) "    %f"))

;; Disable toolbar
(tool-bar-mode 0)

;; Disable menubar
(menu-bar-mode 0)

;; Disable scrollbar
(set-scroll-bar-mode nil)

(global-font-lock-mode t)

;; Disbale cursor blink
(blink-cursor-mode -1)

;; Only enable linum-mode for files less than 10000 lines
(defun turn-on-linum-mode ()
  (when (buffer-file-name)
    (when (or (< (buffer-size) (* 10000 200))
              (< (line-number-at-pos (point-max)) 10000))
      (setq-local display-line-numbers-width 4)
      (display-line-numbers-mode))))
(add-hook 'after-change-major-mode-hook 'turn-on-linum-mode)

;; Highlight the selected area
(transient-mark-mode 1)

;; Use bar style cursor
(setq-default cursor-type 'bar)

;; Highlight the current line
(global-hl-line-mode 1)

;; Highlight bracket at cursor
(show-paren-mode)

;; Scroll the page with the cursor in the same line
(defun hold-line-scroll-up()
  (interactive)
  (let ((tmp (current-column)))
    (scroll-up 1)
    (line-move-to-column tmp)
    (forward-line 1)))

(defun hold-line-scroll-down()
  (interactive)
  (let ((tmp (current-column)))
    (scroll-down 1)
    (line-move-to-column tmp)
    (forward-line -1)))

(global-set-key (kbd "M-n") 'hold-line-scroll-up)
(global-set-key (kbd "M-p") 'hold-line-scroll-down)

(setq isearch-lazy-count t)

(setq content-width 130)

(defvar exclude-buffers '("*Ido Completions*"))

(defun center-content ()
  ;; Center content when width of the window is wide enough.
  (let ((custom-width-margin (/ (- (window-total-width) content-width) 2)))
    (dolist (win (get-buffer-window-list (current-buffer) nil t))
      (unless (or (window-minibuffer-p win)
                  (member (buffer-name (window-buffer win)) exclude-buffers))
        (if (> custom-width-margin 4)
            (set-window-margins win custom-width-margin custom-width-margin)
          (set-window-margins win 0 0))))))
(add-hook 'window-state-change-hook (lambda () (center-content)))
(add-hook 'buffer-list-update-hook (lambda () (center-content)))

(defun increase-content-width ()
  (interactive)
  (setq-local content-width (+ content-width 2))
  (center-content)
  (message "Content width: %s" content-width))

(defun decrease-content-width ()
  (interactive)
  (setq-local content-width (- content-width 2))
  (center-content)
  (message "Content width: %s" content-width))

(defun reset-content-width ()
  (interactive)
  (setq-local content-width 130)
  (center-content)
  (message "Content width: %s" content-width))

(global-set-key (kbd "C-=") 'increase-content-width)
(global-set-key (kbd "C--") 'decrease-content-width)
(global-set-key (kbd "C-+") 'reset-content-width)

(provide 'init-interface)
