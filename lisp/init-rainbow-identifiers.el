(require 'rainbow-identifiers)

(add-hook 'prog-mode-hook 'rainbow-identifiers-mode)

(provide 'init-rainbow-identifiers)
