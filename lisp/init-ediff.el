(setq ediff-window-setup-function 'ediff-setup-windows-plain
      ediff-split-window-function 'split-window-horizontally
      ediff-make-buffers-readonly-at-startup t
      ediff-keep-variants nil
      ediff-diff-options "-w")

(defun disable-ediff-y-or-n-p (orig-fun &rest args)
  (cl-letf (((symbol-function 'y-or-n-p) (lambda (prompt) t)))
    (apply orig-fun args)))
(advice-add 'ediff-quit :around #'disable-ediff-y-or-n-p)

(defvar ediff-window-config nil)
(defun ediff-restore-window-config ()
  (set-window-configuration ediff-window-config))
(defun ediff-save-windows-config ()
  (setq ediff-window-config (current-window-configuration)))
(add-hook 'ediff-before-setup-hook #'ediff-save-windows-config)
(add-hook 'ediff-quit-hook #'ediff-restore-window-config)

;; Disable centaur-tabs in ediff mode.
(add-hook 'ediff-before-setup-windows-hook 'centaur-tabs-local-mode)
(add-hook 'ediff-prepare-buffer-hook 'centaur-tabs-local-mode)

(provide 'init-ediff)
