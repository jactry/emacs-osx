(add-to-list 'load-path "~/.emacs.d/lisp/plugins/benchmark-init/")

(require 'benchmark-init-loaddefs)
(benchmark-init/activate)

(provide 'init-benchmark-init)
