(require 'whitespace)

(whitespace-turn-on)
(global-whitespace-mode t)
(setq whitespace-style (quote (spaces tabs newline space-mark tab-mark newline-mark)))
(setq whitespace-display-mappings
      '((newline-mark 10 [182 10])
        (tab-mark 9 [8677 9] [92 9])))

(provide 'init-whitespace)
