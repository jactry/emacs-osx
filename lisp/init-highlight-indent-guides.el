(require 'highlight-indent-guides)

(add-hook 'emacs-startup-hook
          (lambda ()
            (dolist (buffer (buffer-list))
              (with-current-buffer buffer
                (highlight-indent-guides-mode 0)))))

(setq highlight-indent-guides-method 'character)

(defun toggle-highlight-indent-guides ()
  (interactive)
  (if highlight-indent-guides-mode
      (progn
        (highlight-indent-guides-mode 0)
        (message "highlight-indent-guides-mode has been turned off."))
    (progn
      (highlight-indent-guides-mode 1)
      (run-at-time "10 sec" nil (lambda () (highlight-indent-guides-mode 0))))))

(global-set-key (kbd "C-c l") 'toggle-highlight-indent-guides)

(provide 'init-highlight-indent-guides)
