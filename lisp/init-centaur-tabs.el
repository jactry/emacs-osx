(require 'centaur-tabs)

(setq centaur-tabs-style "bar"
      centaur-tabs-height 32
      centaur-tabs-label-fixed-length 12
      centaur-tabs-set-icons t
      centaur-tabs-icon-type 'all-the-icons
      centaur-tabs-show-new-tab-button nil
      centaur-tabs-cycle-scope 'tabs
      centaur-tabs-set-bar 'left)
(centaur-tabs-headline-match)
(centaur-tabs-mode t)

(centaur-tabs-change-fonts preferred-font-name 1.0)

(defun centaur-tabs-hide-tab (x)
  (let ((name (format "%s" x)))
    (or
     (window-dedicated-p (selected-window))
     (string-prefix-p "*Async-native-compile-log*" name)
     (string-prefix-p "*Disabled Command*" name)
     (string-prefix-p "*Message" name)
     (string-prefix-p "*Native-compile-Log*" name)
     (string-prefix-p "*clang-error*" name)
     (string-prefix-p "*clang-output*" name)
     (string-prefix-p "*scratch" name))))

(defun centaur-tabs-buffer-groups ()
  (list
   (cond
    ((string-equal "*" (substring (buffer-name) 0 1)) "Emacs")
    ((string-prefix-p "*Annotate" (buffer-name)) "VC")
    (t "Code"))))

(if (display-graphic-p)
    (progn (global-set-key (kbd "C-<tab>") 'centaur-tabs-backward)
           (global-set-key (kbd "C-q") 'centaur-tabs-forward)
           (global-set-key (kbd "<prior>") 'centaur-tabs-move-current-tab-to-left)
           (global-set-key (kbd "<next>") 'centaur-tabs-move-current-tab-to-right))
  (progn (global-set-key (kbd "C-<prior>") 'centaur-tabs-backward)
         (global-set-key (kbd "C-<next>") 'centaur-tabs-forward)
         (global-set-key (kbd "C-S-<prior>") 'centaur-tabs-move-current-tab-to-left)
         (global-set-key (kbd "C-S-<next>") 'centaur-tabs-move-current-tab-to-right)))

(if is-macos (global-set-key (kbd "C-S-<tab>") 'centaur-tabs-backward-group)
  (global-set-key (kbd "C-S-<iso-lefttab>") 'centaur-tabs-backward-group))

(global-set-key (kbd "C-c k a") 'centaur-tabs-kill-all-buffers-in-current-group)
(global-set-key (kbd "C-c k o") 'centaur-tabs-kill-other-buffers-in-current-group)

(provide 'init-centaur-tabs)
