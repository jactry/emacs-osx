(defvar is-macos nil)
(if (eq system-type 'darwin) (setq is-macos t))

(when is-macos
  (add-to-list 'default-frame-alist '(ns-appearance . dark))
  (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t)))

(setq package-enable-at-startup nil
      byte-compile-warnings '(cl-functions))

(when is-macos
  (add-to-list 'exec-path "/opt/homebrew/bin/")
  (setenv "LIBRARY_PATH" "/opt/homebrew/opt/gcc/lib/gcc/current:/opt/homebrew/opt/libgccjit/lib/gcc/current:/opt/homebrew/opt/gcc/lib/gcc/current/gcc/aarch64-apple-darwin24/14"))

(provide 'early-init)
