#!/usr/bin/env bash

PLUGINS_PATH="./lisp/plugins"

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

download()
{
    for url in "$@"; do
        filename=`basename $url`
        if [[ $url == *"=>"* ]]; then
            filename=`echo ${url} | awk -F '=>' '{print $2}'`
            url=`echo ${url} | awk -F '=>' '{print $1}'`
            echo $filename
        fi
        echo "Downloading $url ..."
        echo $filename
        wget $url -qO $filename
        if [ $? -ne 0 ]; then
            "Failed to download $url."
        fi
    done
}

update-plugin()
{
    if [ ! -d "$PLUGINS_PATH/$1" ]; then
        echo "Plugin $1 not found."
        exit
    fi
    pushd "$PLUGINS_PATH/$1"
    if [ -f "urls" ]; then
        echo "Updating $1..."
        download `cat urls`
    fi
    popd
}

update-all-plugins()
{
    plugins=`ls $PLUGINS_PATH`
    for plugin in $plugins; do
        update-plugin "$plugin"
    done
}

usage()
{
    echo "$0 usage:";
    echo "	$0 <plugin name>";
    echo "	$0 --all: Update all the plugins.";
}

if [ -z "$1" ]; then
    usage
    exit
fi

if [[ $1 == "-h" || $1 == "--help" ]]; then
    usage
    exit
fi

if [[ $1 == "--all" ]]; then
    update-all-plugins
    exit
fi

update-plugin $1

