## Installation

1. `git clone https://gitlab.com/jactry/emacs.d.git ~/.emacs.d`

2. Startup Emacs, and run `M-x all-the-icons-install-fonts` for installing some icon fonts.
